export interface Config {
    celular: any;
    identificacion: any;
    email: any;
    ciudad: any;
    idrol: any;
    id:any;
    nombre1:string;
    nombre2:string;
    apellido1:string;
    apellido2:string;
    direccion:string;
    _embedded:string;
    show:any;
    image:string;
    idCurso:number;
    IdUsuarioCurso:number;
}
