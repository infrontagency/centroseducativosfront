import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FaqsComponent } from './faqs/faqs.component';
import { HomeComponent } from './home/home.component';
import { WebComponent } from './web.component';

const routes: Routes = [{

  path: '',
  component: WebComponent,
  children: [{
    path: '',
    component: HomeComponent
  },
  {
    path: 'faqs',
    component: FaqsComponent
  }]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class WebRoutingModule { }
