import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Component, Inject, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { DialogRole, MatDialog , MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { ToastrService } from 'ngx-toastr';
import { throwError } from 'rxjs';
import { Config } from 'src/app/interface/config';


@Component({
  selector: 'app-crear-usuario',
  templateUrl: './crear-usuario.component.html',
  styleUrls: ['./crear-usuario.component.scss']
})
export class CrearUsuarioComponent implements OnInit {
  api = "http://18.219.44.100:9999/api";

  constructor(
    private toastr: ToastrService,
    private http: HttpClient,
    public dialog: MatDialog,
    public dialogRef: MatDialogRef<CrearUsuarioComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Config
  ) { }
  profileForm = new FormGroup({
    Nombre1: new FormControl('', Validators.required),
    Nombre2: new FormControl(''),
    Apellido1: new FormControl('', Validators.required),
    Apellido2: new FormControl(''),
    Identificacion: new FormControl('', Validators.required),
    Email: new FormControl('', Validators.required),
    Direccion: new FormControl('', Validators.required),
    Celular: new FormControl('', Validators.required),
    IdRol: new FormControl(''),
    Ciudad: new FormControl(''),
  });
  ngOnInit(): void {
  }
  onSubmitt() {
    console.log("Crear");
    var data = this.profileForm.value;
    data.IdRol = 1;
    this.Post(data);
  }
  Post(data: any) {

    this.http.post(this.api + "/usuarios", data).subscribe(res => {
      this.dialog.closeAll();
      this.toastr.success('Bien, Has guardado el usuario', 'Usuarios');

    }, error => {
      this.toastr.error('No fue posible guardar el usuario', 'Usuarios');

    });
  }

  private handleError(error: HttpErrorResponse) {
    if (error.status === 0) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong.
      console.error(
        `Backend returned code ${error.status}, body was: `, error.error);
    }
    // Return an observable with a user-facing error message.
    return throwError(
      'Something bad happened; please try again later.');
  }
}
