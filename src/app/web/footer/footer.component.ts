import { Component, OnInit } from '@angular/core';

export interface Section {
  name: string;
  updated: Date;
}
@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {
 
 
  constructor() { }

  ngOnInit(): void {
  }

}
