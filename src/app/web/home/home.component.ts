import { ViewChild } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { MatMenuModule, MatMenuTrigger } from '@angular/material/menu';
import { MatSliderModule } from '@angular/material/slider';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';
import { MatDialog, MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { CrearUsuarioComponent } from '../crear-usuario/crear-usuario.component';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Config } from 'src/app/interface/config';
import { EditarUsuarioComponent } from '../editar-usuario/editar-usuario.component';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  displayedColumns: string[] = ['demo-position', 'demo-name', 'demo-weight', 'demo-symbol'];




  typesOfShoes: any;
  services: any;
  api = "http://18.219.44.100:9999/api";
  usuarios: any;
  profileForm = new FormGroup({
    Nombre1: new FormControl('', Validators.required),
    Nombre2: new FormControl(''),
    Apellido1: new FormControl('', Validators.required),
    Apellido2: new FormControl(''),
    Identificacion: new FormControl('', Validators.required),
    Email: new FormControl('', Validators.required),
    Direccion: new FormControl('', Validators.required),
    Celular: new FormControl('', Validators.required),
    IdRol: new FormControl(''),
    Ciudad: new FormControl(''),
  });
  constructor(
    private http: HttpClient,
    private toastr: ToastrService,
    public dialog: MatDialog,
  ) {
    // Create 100 users
    //const users = Array.from({ length: 100 }, (_, k) => createNewUser(k + 1));
    //this.dataSource =   new MatTableDataSource(users);
    // Assign the data to the data source for the table to render
  }


  ngOnInit(): void {
    this.Get();
  }





  public Get() {

    this.http.get<Config>(this.api + "/usuarios").subscribe(res => {
      console.log(res);
      this.usuarios = res;



    }, err => {
      this.toastr.error('No se pudieron obtener los usuarios', 'Usuarios');

    });

  }

  openDialog() {
    const dialogRef = this.dialog.open(CrearUsuarioComponent);

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
      this.Get();
    });
  }
  edit(id: any) {
    const dialogRef = this.dialog.open(EditarUsuarioComponent, {
      data: {
        "id":id
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
      this.Get();
    });

  }
}


