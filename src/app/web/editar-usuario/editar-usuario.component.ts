import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Component, Inject, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { DialogRole, MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ToastrService } from 'ngx-toastr';
import { throwError } from 'rxjs';
import { Config } from 'src/app/interface/config';
export interface DialogData {
  id: any;
}
@Component({
  selector: 'app-editar-usuario',
  templateUrl: './editar-usuario.component.html',
  styleUrls: ['./editar-usuario.component.scss']
})
export class EditarUsuarioComponent implements OnInit {
  api = "http://18.219.44.100:9999/api";
  cursosNuevos: any;
  cursosActivos: any;
  IdUsuario = this.data;
  constructor(
    private toastr: ToastrService,
    private http: HttpClient,
    public dialog: MatDialog,
    public dialogRef: MatDialogRef<EditarUsuarioComponent>,

    @Inject(MAT_DIALOG_DATA) public data: Config
  ) {
    console.log(this.data);
    var IdUser = this.data;
    this.Get(IdUser.id);
    this.GetUsuarioCursos(IdUser.id)

  }
  profileForm = new FormGroup({
    Nombre1: new FormControl('', Validators.required),
    Nombre2: new FormControl(''),
    Apellido1: new FormControl('', Validators.required),
    Apellido2: new FormControl(''),
    Identificacion: new FormControl('', Validators.required),
    Email: new FormControl('', Validators.required),
    Direccion: new FormControl('', Validators.required),
    Celular: new FormControl('', Validators.required),
    IdRol: new FormControl(''),
    Ciudad: new FormControl(''),
  });

  //Inicializamos
  ngOnInit(): void {
    var IdUsuario = this.data.id;

    this.GetCursosNuevos(IdUsuario);

  }
  //Accion del formulario
  onSubmitt() {
    var data = this.profileForm.value;
    data.IdRol = 1;
    var IdUsuario = this.data.id;
    this.Put(IdUsuario, data);
  }

  //Editar usuario
  Put(id: any, data: any) {
    console.log(id);
    console.log(data);
    //const headers = new HttpHeaders({'Content-Type':'application/json; charset=utf-8', "Access-Control-Allow-Origin":"*","Access-Control-Allow-Headers": "Origin, X-Requested-With, Content-Type, Accept"});

    this.http.put(this.api + "/usuarios/" + id, data).subscribe(res => {
      this.dialog.closeAll();
      this.toastr.success('Bien, Has guardado el usuario', 'Usuarios');

    }, error => {
      this.toastr.error('No fue posible guardar el usuario', 'Usuarios');

    });
  }

  //Los usuarios a los cuale el usuario esta inscrito
  public GetUsuarioCursos(id: any) {
    this.http.get<Config>(this.api + "/usuariocursos/" + id).subscribe(res => {
      this.cursosActivos = res;
      console.log(res);
    },
      error => {
        this.toastr.error('No se pudieron obtener los usuarios', 'Usuarios');
      });
  }

  //Los Cursos a los cuales el usuario no esta inscrito
  public GetCursosNuevos(id: any) {
    this.http.get<Config>(this.api + "/usuariocursos/cursosnuevos/" + id).subscribe(res => {
      this.cursosNuevos = res;
      console.log(res);
    },
      error => {
        this.toastr.error('No se pudieron obtener los usuarios', 'Usuarios');
      });
  }

  //Todos los datos del usuario
  public Get(id: any) {

    this.http.get<Config>(this.api + "/usuarios/" + id).subscribe(res => {
      console.log(res);

      this.profileForm = new FormGroup({
        Nombre1: new FormControl(res.nombre1, Validators.required),
        Nombre2: new FormControl(res.nombre2),
        Apellido1: new FormControl(res.apellido1, Validators.required),
        Apellido2: new FormControl(res.apellido2),
        Identificacion: new FormControl(res.identificacion, Validators.required),
        Email: new FormControl(res.email, Validators.required),
        Direccion: new FormControl(res.direccion, Validators.required),
        Celular: new FormControl(res.celular, Validators.required),
        IdRol: new FormControl(res.idrol),
        Ciudad: new FormControl(res.ciudad),
      });

    }, err => {
      this.toastr.error('No se pudieron obtener los usuarios', 'Usuarios');

    });

  }

  //Para inscribir un curso nuevo
  InscribirCurso(IdCurso: any) {
    var IdUsuario = this.data.id;
    console.log(IdCurso);
    var data = {
      "IdUsuario": this.IdUsuario.id,
      "IdCursos": IdCurso
    }
    this.http.post<Config>(this.api + "/usuariocursos/", data).subscribe(res => {

      console.log(res);
      this.GetUsuarioCursos(this.IdUsuario.id);
      this.GetCursosNuevos(this.IdUsuario.id);

    },
      error => {
        this.toastr.error('No se pudieron obtener los usuarios', 'Usuarios');
      });
  }
  //Eliminar una suscribcion a un curso
  EliminarCurso(id: any) {
    var IdUsuario = this.data.id;

    this.http.delete(this.api + "/usuariocursos/" + id).subscribe(er => {
      this.GetUsuarioCursos(this.IdUsuario.id);
      this.GetCursosNuevos(this.IdUsuario.id);
    }, error => {
      this.toastr.error('No se pudo desuscribir el curso', 'Cursos');

    })

  }
  private handleError(error: HttpErrorResponse) {
    if (error.status === 0) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong.
      console.error(
        `Backend returned code ${error.status}, body was: `, error.error);
    }
    // Return an observable with a user-facing error message.
    return throwError(
      'Something bad happened; please try again later.');
  }
}
